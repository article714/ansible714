# Services and Hosts

The idea behind _host_ and _service_ conventions is to ease the management of a file based [inventory](inventory-structure.md) where each `.yml`file contains all the necesary variable definitions to manage a single host/service.

## Definitions

To avoid confusion, we will use the following definitions:

-   a **Node**, is any Ansible [Managed Node](https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#id2), which might also be named _host_ in Ansible documentation.
-   a [Host](#host), refers to an Ansible714 host, i.e. a Node for wich an inventory file exists and includes Ansible714 [metadata](#metadata),
-   a [Service](#service), is an Ansible714 specific "abstract" entity, representing a (potentially) complex deployment of several parts into a single inventory piece.

### Host

:material-information-outline: An **Ansible714 Host**, is essentially a convention to organize inventory and enable running [a714-runner](ansible714-runner.md) targetting a single Node.

It should be defined in a single inventory file which contains [metadata](#metadata) that enable its processing by [ansible714-runner](ansible714-runner.md).
We recommend that inventory file might be named after Host's `inventory_name`.

_Note:_ Hosts and Nodes should have a [machine_type](machine_types.md) defined, in order that playbooks/roles perform actions as expected.

### Service

:material-information-outline: A **Service** is an abstract entity that might be made of several parts which might be deployed accross several _managed nodes_.

We call it a **service** as it might be described simply by the feature (service) it provides, e.g.: "Client B web portal".

It is, by convention, defined in a ^^single inventory file^^ which contains [metadata](#metadata) that enable its processing by [ansible714-runner](ansible714-runner.md).

It is meant to be dealt with using [a714-runner](ansible714-runner.md), thus enabling the definition and management of all the parts from a single source (see examples below).

_We know no equivalent concept in Ansible's world._

## Metadata

Ansible714 conventions add a `__METADATA__` group/section to Ansible inventory files (yaml format)

[Metadata reference](metadata_reference.md).

```yaml
__METADATA__:
    vars:
        a714_metadata:
            name: Gitlab production service
            ansible_name: gitlab
            version: 2.1.4
            ansible_recipes:
                - name: /home/ansible/playbooks/deploy-docker-containers.yml
                  type: playbook
                  delegated: false
                - name: /home/ansible/playbooks/dns-updates.yml
                  type: playbook
                  delegated: true
                - name: ansible714.ovh.ip_load_balancer
                  type: role
                  delegated: true
                  become: false
                  gather_facts: false
                  hosts: iplb-prod1
```

## Sample configuration

The following schema describes a simple situation (_left part_) where a webserver webserver ([resty](https://github.com/go-resty/resty) based) is run inside a Docker container that is hosted by a virtual machine (`one_node` node/host).

![SimpleExample](img/simple_example.png "simple example"){width="500" }

When building Ansible inventory for that configuration (_right part of the schema_), we write three `.yml` files:

-   one for all the common node variables that will be used by Ansible714 host and service _(`host_vars/one_node.yml`)_
-   one to specify what playbooks/roles will be apply on node, thus defining an Ansible714 host _(`hosts/one_node.yml`)_
-   one to describe the service, and so define how to deploy and manage our webserver (`services/webserver_svc.yml`)

The content of `one_node.yml`, in `host_vars` directory which defines variables for `one_node` Ansible managed node:

```yaml
ansible_connection: ssh
ansible_user: someuser
ansible_ssh_pass: somepassword
ansible_become_password: somepassword
ansible_python_interpreter: /usr/bin/python3
machine_type: "host"
```

The content of `one_node.yml`, which defines the Ansible714 host that _wraps_ `one_node` Ansible managed node:

```yaml
__METADATA__:
    vars:
        a714_metadata:
            name: one_node
            description: My Physical Node
            version: 1.7.0
            ansible_recipes:
                - name: /home/ansible/playbooks/dns-updates.yml
                  type: playbook
                  delegated: true
                - name: docker-node
                  type: role
                  hosts: all

MY_GROUP_OF_HOSTS:
    hosts:
        one_node:
            # DNS configuration
            dns_entries:
                - { name: "one_node", zone: "my-project.net", kind: "A", value: "1.2.3.5" }
children:
    DockerNodes:
        hosts:
            one_node:
```

The content of `webserver_svc.yml`, which defines the Ansible714 service `webserver_svc` that runs inside a Docker container (`webserver_container`) hosted by `one_node` Ansible managed node:

```yaml
__METADATA__:
    vars:
        a714_metadata:
            name: webserver_svc
            description: My WebServer Service
            version: 1.0
            ansible_recipes:
                - name: /home/ansible/playbooks/dns-updates.yml
                  type: playbook
                  delegated: true
                - name: /home/ansible/playbooks/deploy-docker-containers.yml
                  type: playbook
                  delegated: false
                - name: webserver-node
                  type: role
                  delegated: false
                  gather_facts: false
                  hosts: webserver_container

MY_GROUP_OF_HOSTS:
    hosts:
        # webserver container
        webserver_container:
          ansible_remote_user: root
          ansible_remote_addr: webserver_container
          ansible_user: root
          ansible_connection: docker
          machine_type: "container"
          ansible_docker_extra_args: "-H=tcp://one_node.my-project.net:2373 --tls --tlscacert=certs/mydocker-cert.pem --tlscert=keys/clientcert.crt --tlskey=keys/clientcert.key"
          use_ufw: false
          letsencrypt_enable: false
          http_server_flavor: 'resty'
        # Docker runtime hosting node
        one_node:
            letsencrypt_enable: false
            DOCKERIZED_APPS:
              - {
                  "name": "webserver_container",
                  "docker_registry": "docker.io",
                  "image_name": "article714/resty-container",
                  "container_network": "docker",
                  "container_ip": "10.10.11.12",
                  "volumes":
                    [
                      "webserver_logs:/var/log",
                      "webserver_config:/container/config",
                    ],
                  "ports": [80:8080],
                }
            dns_entries:
                - { name: "my-web-service", zone: "my-project.net", kind: "A", value: "1.2.3.5" }
  children:
    DockerNodes:
      hosts:
        one_node:
```
