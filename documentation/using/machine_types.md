# What are "machine types" ?

A **machine type** for Ansible714 is a value that you need to provide in your managed node configuration, in order
for roles/playbooks to adapt actions for each kind of target:

- `container`, a docker container, which is using _runit_ as a service controller utility (#TOFIX we need to work on that)
- `host`, actual physical machine or virtual machine
- `lxd`, a LXD container (quite different from Docker containers)
- `ovh-pci`, an OVH Public Cloud Instance, which is also a virtual machine (`ovh-pci` is a kind of a sub-class of `host`)

Machine types are used troughout Ansible714 playbooks and roles to adapt actions to be taken on the host depending on its nature. E.g.:

- a `container` is not supposed to use/host [systemd](https://systemd.io/), but a `host` is;
- a `container`does not need to define firewall ([iptables](https://fr.wikipedia.org/wiki/Iptables), [nftables](https://en.wikipedia.org/wiki/Nftables)
  , [ufw](https://wiki.ubuntu.com/UncomplicatedFirewall)) rules, but a `host` needs to be protected from network attacks;
- ...

Its a convention that prevents modules/roles authors to define per-role or per-playbook variables/paremeters when they need to
adapt the list of tasks to run for given class/category of hosts.
