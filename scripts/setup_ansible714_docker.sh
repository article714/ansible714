#!/bin/bash

#------------------------------------
show_usage() {
    echo "USAGE: setup_ansible714_docker.sh <directory name (full path)>"
}

if [ $# -eq 0 ] || [ $# -gt 1 ]; then
    show_usage
else
    target_dir="$1"
    #------------------ CHECKS --------------------
    if [ ! -d $target_dir ]; then
        echo "ERROR: target directory (${target_dir}) does not exist!"
        exit 1
    fi
    have_wget=$(type wget)
    if [ $? -gt 0 ]; then
        echo "ERROR: cannot find wget, needed to download initialisation content!"
        exit 1
    fi

    #------------------ DIR LAYOUT ------------------------
    echo "Create ${target_dir} directory layout"
    mkdir -p ${target_dir}/inventory/files
    mkdir -p ${target_dir}/inventory/group_vars
    mkdir -p ${target_dir}/inventory/hosts
    mkdir -p ${target_dir}/inventory/host_vars
    mkdir -p ${target_dir}/inventory/certs
    mkdir -p ${target_dir}/inventory/keys
    mkdir -p ${target_dir}/inventory/services
    mkdir -p ${target_dir}/log
    mkdir -p ${target_dir}/playbooks
    mkdir -p ${target_dir}/scripts
    mkdir -p ${target_dir}/tools

    #---------------------------------------------
    echo "Get run_ansible script"
    cd ${target_dir}/scripts
    wget -q  https://gitlab.com/article714/ansible714/-/raw/master/scripts/run_ansible714.sh
    chmod +x ${target_dir}/scripts/*
    echo "Copy sample configuration files"
    cd ${target_dir}
    wget -q  https://gitlab.com/article714/ansible714/-/raw/master/documentation/examples/ansible.cfg
    wget -q  https://gitlab.com/article714/ansible714/-/raw/master/documentation/examples/.gitignore
    cd inventory
    wget -q  https://gitlab.com/article714/ansible714/-/raw/master/documentation/examples/hosts.yml
fi
