# Ansible714: Ansible toolings used by Article714 project

| :warning: WARNINGS                                                                                                                                 |
| -------------------------------------------------------------------------------------------------------------------------------------------------- |
| Ansible714 Documentation is still a work in progress :material-account-hard-hat:. It is incomplete and only partially translated (English/French). |

## Introduction

**Ansible714** is an _Infrastructure As Code_ toolbox based on [Ansible](https://ansible.com).

It is an opinionated distribution of [Ansible](https://ansible.com) which includes components:

-   chosen from the huge catalog of community projects,
-   developed by the Ansible714 project for specific needs.

It is used to automate the deployment and maintenance of an infrastructure:

-   of "small" size (a few tens of nodes),
-   using mainly Opensource software,
-   preferably targeting Linux distributions of the Debian family ([Debian](https://www.debian.org/), and [Ubuntu](https://ubuntu.com/)),
-   leveraging [Docker](https://www.docker.com/), [Openstack](https://www.openstack.org/) and/or [Kubernetes](https://kubernetes) .io/fr/) for the deployment of services,
-   hosted by an IaaS provider such as [OVH Cloud](https://www.ovhcloud.com/fr/)[^1].

In particular Ansible714 is used to manage environments hosting services developed/packaged by [Article714](https://www.article714.org) project.

**Ansible714 tooling is not compatible with python 2.x**

## Similar projects

-   [A:Platform64](https://aplatform64.readthedocs.io), _"A:Platform64 is an automated infrastructure-as-code management platform based on Ansible."_
-   [DebOps](https://docs.debops.org), _Your Debian-based data center in a box_
-   [Sovereign](https://github.com/sovereign/sovereign), _"Sovereign is a set of Ansible playbooks that you can use to build and maintain your own personal cloud based entirely on open source software, so you’re in control."_

## Python compatibility

This tooling is compatible with python 3.8+.
_Efforts are being made to test it against earlier version of Python, but there is no warranty that everything works_

Python 2 has been deprecated [1st January 2020](https://www.python.org/doc/sunset-python-2/), we should all consider switching to Python 3.

---

[^1]: Currently, only OVH Cloud services deployment are partially automated, in particular via [ansible714-ovh-collection](https://gitlab.com/article714/ansible714-ovh-collection).
