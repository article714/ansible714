# About Ansible714 development (and how to contribute)

## Evolving/Fixing Ansible714

_Development dependencies (tooling, linters,...) are defined in the `requirements_dev.txt` file._

## Issue Management

_ILC stands for Issues LifeCycle._

:: _ILC::Reviewing_ step is for issues that need some more information, details or specification
before being considered ready to be worked on

:: _ILC::Todo_ step is for issues ready to be worked on/planned

- _ILC::Doing_ step is for issues being actively worked on
- in _ILC::Done_ state, the issue is considered to be "fixed", and work has been merged on _master_,
   it is then included to the `preview` version
- _Closed_ issues are the ones for which all the work has already been merged on _production_ and
  included to the `latest` version

## Branching

Three main branches are used to develop Ansible714:

- **master**, main development branch, used for publishing _:preview_ docker images
- **production**, used for publishing _:latest_ docker images
- **test**, used for developments requiring to skip certain phases of the build

Official releases (except _:latest_) are produced from a **tag** in the format _X.Y.Z_.

## Building (CI/CD)

The Ansible714 project is automatically built using [Gitlab's CI/CD](https://docs.gitlab.com/ee/ci/)
, thus the presence in source code of the `.gitlab-ci.yml`file.

Successful Pipelines execution depends on a properly configured set of [gitlab runners](https://docs.gitlab.com/runner/):

- at least one [Docker runner](https://docs.gitlab.com/runner/register/#docker), defined with a single tag: `docker`
- at least one [Shell runner](), defined with a single tag: `shell`
- at least one untagged `Docker` runner, used by Gitlab security scanners (eg. [IAC scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/))

Furthermore, the building host should also provide:

- a working [Docker](https://docs.docker.com/engine/install/) installation
- a [Python 3.x](https://www.python.org) interpreter, with [pip](https://pip.pypa.io/en/stable/)

### Building on your own infrastructure

**TODO**: describe how and why...

## Release Management

_TODO_: a lot to write here

### Versioning

Starting from Ansible714 version 0.2.1, the new versioning scheme for images will be:
`${ANSIBLE_VERSION}-py${PYTON_VERSION}-${DEBIAN_VERSION}-${ANSIBLE714_TOOLING_VERSION}`

### Images publishing

To _prevent image to be pushed_ to docker public registry, you can setup a [Gitlab CI/CD project variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
: `DO_NOT_PUBLISH`. If set (whatever the value) it will skip image publishing steps.

## Dependency management

Dependencies in Ansible714 are handled in different ways depending on their nature and where they come from.
6 files used to define them:

- three for dependencies to python modules (in [pip requirements file](https://pip.pypa.io/en/stable/reference/requirements-file-format/)
   format):

  - `requirements.txt`: runtime dependencies of the Ansible714 tooling, also necessary for the
    development phases;

  - `requirements_dev.txt`: dependencies towards modules/tools necessary for the development of Ansible714;

  - `requirements_docs.txt`: dependencies towards modules useful only for documentation processing;

- two for dependencies to Ansible roles/collections:

  - `galaxy_requirements.txt`: dependencies installable by [ansible-galaxy](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html);

## Documentation

To _prevent documentation container to be deployed_ to your environment (if [building on your own infrastructure](#building-on-your-own-infrastructure)
) you can setup a [Gitlab CI/CD project variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
: `DO_NOT_DEPLOY_DOC`. If set (whatever the value) it will skip documentation publishing steps.

## Development scripts

The directory `tests/scripts`contains the following scripts that may be used for developing/testing on your local workstation:

- `build_docs_locally.sh`: builds a container serving built documentation for review;
- `build_local_test_env.sh`: builds a local test environment, running target containers needed to run tests;
- `build_local_test_image.sh`: builds an Ansible714 container image for local test, using current local state of sourcecode;
- `run_local_tests.sh`: run local tests using previously built _localtest_ container image (`build_local_test_image.sh`).
- `run_local_lint.sh`: run `ansible-lint` on current source code using previously built _localtest_ container image (`build_local_test_image.sh`).

All the scripts need a working Docker setup. Their behavior might be customized by defining environment variables:

- `PYTHON_VERSION`, version of python to use,
- `DEBIAN_VERSION`,
- `ANSIBLE_VERSION`, targeted Ansible version.

E.g. if you want to build a _localtest_ container image using Python 3.10 and Ansible 6.5.0 based on \*bullseye-slim" Debian image:

  ```bash
  export PYTHON_VERSION="3.10"
  export DEBIAN_VERSION="bullseye-slim"
  export ANSIBLE_VERSION="6.5.0"

  ./tests/scripts/build_local_test_image.sh

  ```

## Setting up a development environment (virtualenv)

**NOTE**: in order for pre-commit hooks to work, the directory where you will checkout sourcecode (typically `ansible714`)
must have a `775` mode.

1. Install python tooling: `virtualenv`, `virtualenvwrapper` and `python3`

    /e.g. on a Debian or Ubuntu node:
    `apt-get install virtualenv virtualenvwrapper python3`

2. Setup `virtualenvwrapper` (see [official documentation](https://virtualenvwrapper.readthedocs.io/en/latest/install.html))

3. Create your virtual python environment (_virtualenv_):

    ```bash
    # setup  myansiblevenvname to whatever you want to name your venv
    mkvirtualenv -p /usr/bin/python3 $myansiblevenvname
    ```

4. Clone Ansible714 source code

    ```bash
    git clone https://gitlab.com/article714/ansible714.git
    ```

5. Install dependencies in virtualenv, after choosing wich ansible version (_from
   [supported Ansible verions](ansible_versions.md)_) you want to target

    ```bash
    # get in the right directory
    workon $myansiblevenvname

    # enter Ansible714 source code directory
    cd ansible714

    # update pip
    python -m pip install --upgrade pip

    # Choose a targeted ansible version:
    export ANSIBLE_VERSION="6.0.0"

    # install Ansible714 runtime dependencies (python packages)
    python -m pip install --upgrade -r requirements.txt

    # install Ansible714 development deps
    python -m pip install --upgrade -r requirements_dev.txt
    ```

6. Install Galaxy (Ansible) dependencies
   _Depencies should be updated each time you modify requirements file_

    ```bash
    # Collections
    ansible-galaxy collection install -r galaxy_requirements.yml
    # And roles
    ansible-galaxy role install -r galaxy_requirements.yml
    ```

7. Activate [pre-commit](https://pre-commit.com/) hooks

    ```bash
    pre-commit install
    ```
