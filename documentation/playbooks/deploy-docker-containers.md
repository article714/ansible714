---
title: deploy-docker-containers
tags:
    - docker
    - playbook
---

# Management of Docker Containers

This tooling uses [Ansible](https://docs.ansible.com/)'s [Docker Container](https://docs.ansible.com/ansible/2.9/modules/docker_container_module.html) module to provide a way to manage Docker containers using ansible playbooks.

Containers to be deployed (created/updated, deletion is not yet supported), are to be defined using the `DOCKERIZED_APPS` variable _(should be node specific)_. The variable must contain a list of dictionaries (yaml format) containing the parameters to be used to configure the `docker_container` Ansible module. Each entry in the list will address the configuration of a single container to be deployed.

To communicate with Docker daemon, you will have to provide its address using the `DOCKER_HOST` variable which defaults to `'unix://var/run/docker.sock'`.

Following is a short illustration of what `DOCKERIZED_APPS` content could be:

```yaml
DOCKERIZED_APPS:
    - {
          "name": "my-gitlab-container",
          "docker_registry": "docker.io",
          "image_name": "gitlab/gitlab-ee:14.0.0-ee.0",
          "container_network": "internal_bridge",
          "container_ip": "10.10.1.4",
          "hostname": "gitlab.infra.article714.org",
          "recreate": "no",
          "volumes":
              [
                  "gitlab_log:/var/log/gitlab",
                  "gitlab_opt:/var/opt/gitlab",
                  "gitlab_etc:/etc/gitlab",
              ],
      }
```

Next section provides details on properties that can/must be defined for each container.

## Configuration properties

For each container to manage, the following properties can be defined (\* means mandatory):

-   `name`: the name of the Docker container to deploy
-   `docker_registry`: registry where to look at container image, defaults to `'docker.io'`
-   `image_local`: is the image to use only available locally on target node, defaults to `False`
-   `image_name`: the OSI/Docker image to use
-   `volumes`: list of volumes (Docker syntax) to mount in the container
-   `environment`: list of environment variables to setup
-   `command`: command to execute inside container
-   `hostname`: hostname to setup inside the container
-   `ports`: ports to we forwarded
-   `exposed_ports`:
-   `container_network`: which docker network to connect the container to
-   `container_ip`: IP address to use for the container
-   `networks`: list of networks to connect to, when set `container_network` & `container_ip` are not used
-   `dns`: default DNS server to use in the container
-   `capabilities`:
-   `log_driver`: specification of the log driver
-   `log_options`: array of specific logging driver options
-   `shm_size`:
-   `privileged`: is the container a privileged one?
-   `recreate`: defaults to `'yes'`
-   `connect_host_network`: when provided, `networks`,`container_network`,`container_ip` are not used
