#!/bin/bash

# set -x

# setup used paths

MY_SCRIPT=$(realpath -s "$0")
MY_SCRIPTPATH=$(dirname "$MY_SCRIPT")
DOCKER_COMPOSE_PATH=$(realpath -s "$MY_SCRIPTPATH"/../docker)

# Docker Env
project_name="ansible714"

# Docker Commands

DOCKER_CMD="docker-compose -p ${project_name} -f tests/docker/docker-compose.yml exec ansible714_controller /bin/bash -c "
DOCKER_ROOT_CMD="docker-compose -p ${project_name} -f tests/docker/docker-compose.yml exec -u root ansible714_controller /bin/bash -c "

# test list
# tests=("ansible-playbook -i /home/ansible/ansible714/tests/targethost_inventory.yml /home/ansible/playbooks/update-software.yml"
# "ansible-playbook -i /home/ansible/ansible714/tests/localhost_inventory.yml /home/ansible/ansible714/tests/playbooks/test_dns_loop.yml "
# "cd /home/ansible/ansible714;mkdir -p /mnt/tests/a714_logs/svc_to_backup;a714-runner --file tests/svc_to_backup.yml --output-dir /mnt/tests/a714_logs/svc_to_backup --mode backup  --verbosity 3")

tests=(
"docker ps"
"ansible-playbook -i /home/ansible/ansible714/tests/targethost_inventory.yml /home/ansible/playbooks/update-software.yml"
"ansible-playbook -i /home/ansible/ansible714/tests/localhost_inventory.yml /home/ansible/ansible714/tests/playbooks/test_dns_loop.yml "
"cd /home/ansible/ansible714;mkdir -p /mnt/tests/a714_logs/simple_svc;a714-runner --file tests/simple_svc.yml --output-dir /mnt/tests/a714_logs/simple_svc --no-ansible-output;"
"cd /home/ansible/ansible714;mkdir -p /mnt/tests/a714_logs/host_to_backup;a714-runner --file tests/host_to_backup.yml --output-dir /mnt/tests/a714_logs/host_to_backup --mode backup  --verbosity 1"
"cd /home/ansible/ansible714;mkdir -p /mnt/tests/a714_logs/svc_to_backup;a714-runner --file tests/svc_to_backup.yml --output-dir /mnt/tests/a714_logs/svc_to_backup --mode backup  --verbosity 0"
)

env_exists=$(docker-compose ls | grep ${project_name} || true)


#--------------------------
# Let's Go and start the testing env

if [ -f "$DOCKER_COMPOSE_PATH/docker-compose.yml" ]; then
    docker-compose -p ${project_name} -f "${DOCKER_COMPOSE_PATH}"/docker-compose.yml up  -d
else
    echo "FAIL: cannot find docker-compose file"
    exit 1
fi

#------
# Wait a few seconds for env to be actually up
sleep 5



if [ -z "${env_exists}" ]; then
  #----
  # change directory rights

  ${DOCKER_ROOT_CMD} "chown ansible. /mnt/tests"

  # adds ansible to docker group (1001)
  ${DOCKER_ROOT_CMD} "groupadd -g 1001 docker"
  ${DOCKER_ROOT_CMD} "addgroup ansible docker"
  ${DOCKER_ROOT_CMD} " echo '' >> /container/config/ansible.cfg"
  ${DOCKER_ROOT_CMD} " echo '# Remote tmp in /tmp' >> /container/config/ansible.cfg"
  ${DOCKER_ROOT_CMD} " echo 'remote_tmp     = /tmp/.ansible/tmp' >> /container/config/ansible.cfg"
fi

#--------------------
# tests

if [ "$1" == "" ]; then
    for test_cmd in "${tests[@]}"; do
        echo " **************** START NEW TEST ********************"
        echo "Running [$test_cmd]"
        ${DOCKER_CMD} "$test_cmd"
        result=$?
        echo ""
        echo "Finished [$test_cmd]"
        if [ $result -ne 0 ]; then
            echo " **************** TEST FAILED ********************"
            exit 1
        else
            echo " **************** TEST OK ********************"
        fi
    done
else
    ${DOCKER_CMD} "$1"
fi


#--------------------------
# Let's stop the testing env


if [ -f "$DOCKER_COMPOSE_PATH/docker-compose.yml" ]; then
    docker-compose -p ${project_name} -f "${DOCKER_COMPOSE_PATH}"/docker-compose.yml stop
else
    echo "FAIL: cannot find docker-compose file"
    exit 1
fi