#!/bin/bash

# set -x

script_dir=$(dirname "$(realpath "$0")")
source_dir=$(realpath "${script_dir}"/../..)
doc_dir="${source_dir}/documentation"
collections_doc_dir="${doc_dir}/collections"
templates_dir="${doc_dir}/templates"

collections_list=$(cat "${collections_doc_dir}"/collections_list.txt)

for collection in ${collections_list}; do
    mkdir -p "${collections_doc_dir}"/"${collection}";
    modules=$(ansible-doc -t module --list | grep "${collection}" | cut -d ' ' -f 1);
    # shellcheck disable=SC2086
    modules_list=$(printf '"%s",' $modules| sed s/,\$//);
    roles=$(ansible-doc -t role --list | grep "${collection}" | cut -d ' ' -f 1);
    # shellcheck disable=SC2086
    roles_list=$(printf '"%s",' $roles| sed s/,\$//);
    echo "{\"collection_name\":\"$collection\", \"modules\":[$modules_list], \"roles\":[$roles_list]}" > "${collections_doc_dir}"/"${collection}"/collection_data.json
    jinja -d "${collections_doc_dir}"/"${collection}"/collection_data.json "${templates_dir}"/collection_index.yml.j2  > "${collections_doc_dir}"/"${collection}"/index.md
    for module in ${modules}; do
        ansible-doc --type module --json "${module}" | sed -e "s/\"$module\":/\"data\":/" >  "${collections_doc_dir}"/"${collection}"/module_"${module}".json
        jinja -d  "${collections_doc_dir}"/"${collection}"/module_"${module}".json "${templates_dir}"/module.yml.j2 > "${collections_doc_dir}"/"${collection}"/module_"${module}".md
    done;
done;
