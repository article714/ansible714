#!/bin/bash

# set -x

# Let's Go

INTIAL_PWD=$(pwd)

apt-get update
apt-get install -y locales

# Generate French locales
localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

export LANG=en_US.utf8
export LC_ALL=en_US.utf8

# Update base software

apt-get update
apt-get -yq full-upgrade

# Install some basic deps


apt-get install -yq --no-install-recommends \
    ca-certificates \
    curl \
    dialog \
    dnsutils \
    gcc \
    git \
    gpg \
    inetutils-ping \
    iproute2 \
    libc6-dev \
    openssh-client \
    rsync \
    sshpass \
    sudo \
    unzip

# Additional packages source (Docker / Kubernetes)

curl -sL https://download.docker.com/linux/debian/gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/docker.gpg >/dev/null || exit 1
echo 'deb [arch=amd64] https://download.docker.com/linux/debian  buster  stable' >/etc/apt/sources.list.d/docker.list

curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/googlecloud.gpg >/dev/null || exit1
echo "deb [arch=amd64] https://apt.kubernetes.io/ kubernetes-xenial main" >/etc/apt/sources.list.d/kubernetes.list

# Install Ansible714 deps
apt-get update

apt-get install -yq --no-install-recommends \
    docker-ce-cli \
    kubeadm \
    kubectl

# Install rclone
curl https://rclone.org/install.sh | bash


# Install Helm

cd /tmp || exit 1
curl -fsSL -o get-helm-3 https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod +x get-helm-3
./get-helm-3

cd "${INTIAL_PWD}" || exit 1

# checks that some needed commands are installed
if ! type docker;  then
    echo "Cannot find Docker command"
    exit 1
fi
if ! type kubectl; then
    echo "Cannot find KubeCTL command"
    exit 1
fi
if ! type helm; then
    echo "Cannot find HELM command"
    exit 1
fi
if ! type nslookup; then
    echo "Cannot find KubeCTL command"
    exit 1
fi
if ! type rclone;  then
    echo "Cannot find rclone command"
    exit 1
fi

# Install pip dependencies
python3 -m pip install --upgrade pip setuptools setuptools-scm

# ANSIBLE user should have a > 1000 gid to ease uid/gid mapping in docker
addgroup --gid 6666 ansible

adduser --system --home /home/ansible --gid 6666 --uid 6666 --quiet ansible

if [ -d /home/ansible ]; then
   chown -R ansible. /home/ansible
fi

# Setup configuration
ln -f -s /container/config /etc/ansible

# set /container/log to belong to ansible user
mkdir /container/log
chown -R ansible. /container/log
chmod -R 775 /container/log

# Deploying ansible714 Tooling
mkdir -p /home/ansible/ansible714
cd /home/ansible/ansible714  || exit 1
if [ -f /container/ansible714.tar.gz ]; then
    if ! tar xzf /container/ansible714.tar.gz; then
        echo "Could not deploy Ansible714 tooling in container: unable to unarchive"
        exit 1
    fi
    rm -f /container/ansible714.tar.gz
else
    echo "Could not deploy Ansible714 tooling in container: file does not exist"
    exit 1
fi

# install ansible 714 Dependencies (PIP)
python3 -m pip install -r /home/ansible/ansible714/requirements.txt

# checks that some necesary commands are installed
if ! type ansible; then
    echo "Cannot find Ansible command"
    exit 1
fi

# init ansible 714 Dependencies (Galaxy and git repos)
if ! sudo -u ansible /home/ansible/ansible714/scripts/init_dependencies.sh /home/ansible; then
    echo "Could not install dependencies from Galaxy"
    exit 1
fi
cd / || exit 1

# setup scripts & playbooks directory
ln -s /home/ansible/ansible714/scripts /home/ansible/scripts
ln -s /home/ansible/ansible714/playbooks /home/ansible/playbooks

#--
# Removing useless packages

apt-get -yq purge autoconf automake cpp cpp-10 gcc gcc-10 g++ g++-10 libc6-dev x11-common
apt-get -yq autoremove

#--
# Cleaning

apt-get -yq clean
rm -rf /var/lib/apt/lists/*

# Python3 interpreter should be latest one (/usr/local/bin)
ln -fs /usr/local/bin/python3 /usr/bin/python3
