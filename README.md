[![Build Status](https://gitlab.com/article714/ansible714/badges/master/pipeline.svg)](https://gitlab.com/article714/ansible714)

# Ansible714

**Ansible714** is an _Infrastructure As Code_ toolbox based on [Ansible](https://ansible.com).

It is an opinionated distribution of [Ansible](https://ansible.com) which includes components:

-   chosen from the huge catalog of community projects,
-   developed by the Ansible714 project for specific needs.

It is used to automate the deployment and maintenance of an infrastructure:

-   of "small" size (a few tens of nodes),
-   using mainly Opensource software,
-   preferably targeting Linux distributions of the Debian family ([Debian](https://www.debian.org/), and [Ubuntu](https://ubuntu.com/)),
-   leveraging [Docker](https://www.docker.com/), [Openstack](https://www.openstack.org/) and/or [Kubernetes](https://kubernetes) .io/fr/) for the deployment of services,
-   hosted by an IaaS provider such as [OVH Cloud](https://www.ovhcloud.com/fr/)[^1].

In particular Ansible714 is used to manage environments hosting services developed/packaged by [Article714](https://www.article714.org) project.

Ansible714 is an opinionated distribution of [Ansible](https://ansible.com), packaged with some recipes (roles, playbooks, ...).

It is used to manage environments where some other software developed / packaged by the [Article714](https://www.article714.org) project are deployed.

The overall packaged (i.e. any source code in this repository) is delivered under the [GPL V3](https://www.gnu.org/licenses/gpl-3.0.en.html) License.

Some documentation is available in the [documentation](https://gitlab.com/article714/ansible714/-/tree/master/documentation) directory.
It is also published on [Ansible714 website](https://ansible714.article714.org).

## Python compatibility

This tooling is compatible with python 3.8+.
_Efforts are being made to test it against earlier version of Python, but there is no warranty that everything works_

Python 2 has been deprecated [1st January 2020](https://www.python.org/doc/sunset-python-2/), we should all consider switching to Python 3.
