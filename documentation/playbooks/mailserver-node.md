---
title: mailserver-node
tags:
    - mail
    - postfix
    - playbook
---

# Manage a mailserver (SMTP, IMAP) configuration
