#/!/bin/bash

# set -x

#
# Script to backup a Docker Volume.
#
# Takes 3 arguments, of which the first 2 are mandatory:
#   * the name of the volume to backup
#   * the directory on the host where to store backup
#
# And 1 optional arg which drives how script returns
#   - if no arg or 'fail' => fails if there is an error
#   - if 'nofail' => no fail (exit 0) even if there is an error
#   - if anything else => fails if error
#
# DEFAULT for Tar is to exclude directory contents when dir contains a .NOBACKUP.INFO file
#

FROM=$1
TO=$2
FAIL=$3

laDate=$(date +%d%m%Y)

#
if [ -z "${TAR_EXCLUDES}" ]; then
    EXCLUDE_OPT="--exclude-tag-under=.NOBACKUP.INFO"
else
    EXCLUDE_OPT="--exclude-tag-under=.NOBACKUP.INFO"
    for adir in ${TAR_EXCLUDES}; do
        EXCLUDE_OPT="--exclude=${adir} ${EXCLUDE_OPT}"
    done
fi

docker run --rm -v "${FROM}":/from-volume -v "${TO}":/to-volume ubuntu /bin/bash -c "cd /from-volume && tar -z --ignore-failed-read --warning=no-file-changed --create ${EXCLUDE_OPT} --file  /to-volume/${1}_${laDate}_archive.tar.gz ."

result=$?

if [ "${FAIL}" = "nofail" ]; then
    if [ ${result} -eq 2 ]; then
        exit 1
    else
        exit 0
    fi
else
    exit ${result}
fi
