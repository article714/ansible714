# Secrets management for Ansible714

Ansible714 is pre-loaded with the tools (python libraries, ansible plugins, cli) needed to use [Hashicorp Vault](https://www.vaultproject.io/)
as a repository for any secret needed when running Ansible playbooks.

We found [Hashicorp Vault](https://www.vaultproject.io/) to be an efficient alternative to managing secrets with [Ansible vault](https://docs.ansible.com/ansible/latest/vault_guide/index.html).
Ansible714 container image embeds :

* [hashi_vault](https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/index.html)
  collection, that provides several modules to use Hashicorp vault from your roles/playbooks;
* [hvac](https://github.com/hvac/hvac) Python library, to enable writing Ansible modules interacting
  with Vault.

E.G, the `hashi_vault` lookup plugin makes it very simple to retrieve secrets from a vault:

```yaml
# retrieving `value`key of secret stored in a KV mount at `basepath/my_secret`
# using hashi_vault lookup plugin.
some_secret: |-
  {{ lookup('community.hashi_vault.hashi_vault',
  'basepath/data/my_secret',
  cacert=inventory_dir~'/certs/lease4-sca-two-chain.pem')['value'] }}
```
