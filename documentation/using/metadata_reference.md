# Metadata reference

List of metadata properties used by [ansible714-runner](ansible714-runner.md) to define [services](services_hosts.md#service) and [hosts](services_hosts.md#host).

```yaml
__METADATA__:
    vars:
        a714_metadata:
            name: MyBeautifulService
            version: 1.0.0
            description: My service that helps users
            backup_node: some_backup_node
            ansible_recipes:
                - name: somefile.yml
                  type: playbook
                  delegated: false
                  become: false
                  gather_facts: false
                  hosts: iplb-prod1
                  mode: any
```

```yaml
# Short name of the service, should not contain whitespace or special characters, inventory file must be named after service's name
name: MyBeautifulService
```

```yaml
# version of file content (configuration)
version: 1.0.0
```

```yaml
# free description of service
description: My service that helps users
```

```yaml
# inventory_name of the node on which service backup files will be stored
backup_node: some_backup_node
```

```yaml
# array of recipes to be applied on target(s)
ansible_recipes:
```

```yaml
# Name of the playbook file to execute ou role to apply
- name: somefile.yml
```

```yaml
# kind of recipe to apply in ["playbook","role"]
type: playbook
```

```yaml
# Delegate role execution to "localhost", default value: false
delegated: false
```

```yaml
# Should Ansible 'sudo' to execute recipe
become: false
```

```yaml
# Do we need to gather facts (same as Ansible)
gather_facts: false
```

```yaml
# List of hosts on which recipe will be applied
hosts: iplb-prod1
```

```yaml
# mode is used to filter recipes to appy, default value: none, special values: any (run if any mode is specified), none (run if no mode is specified)
mode: any
```
