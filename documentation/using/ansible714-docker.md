# Using Docker image

A Docker image for [Ansible](https://ansible.com) including [Ansible714](https://gitlab.com/article714/ansible714) tooling.

That image can be used to run any [Ansible](https://ansible.com) command using an [inventory](inventory-structure.md)
that can be initialized as such:

```bash
export inventory_dir='/to/be/customized'

wget https://gitlab.com/article714/ansible714/-/raw/master/scripts/setup_ansible714_docker.sh

chmod +x setup_ansible714_docker.sh

./setup_ansible714_docker.sh ${inventory_dir}
```

Then, you can use the provided **wrapper script** to run commandes inside an ansible714-docker container:

```bash
export inventory_dir='/to/be/customized'

cd ${inventory_dir}

./scripts/run_ansible714.sh ansible --version

./scripts/run_ansible714.sh ansible all -m ping
```

The wrapper script uses\_ **6.0.0-py3.10-bullseye-latest** \_version of Ansible714 by default. To
use any specific version, you can either edit the downloaded script or define `ANSIBLE714_VERSION`
environment variable:

```bash
export inventory_dir='/to/be/customized'
export ANSIBLE714_VERSION='2.9.27-py3.10-slim-bullseye-preview'

cd ${inventory_dir}

# show ansible version
./scripts/run_ansible714.sh ansible --version

# ping all hosts in inventory
./scripts/run_ansible714.sh ansible all -m ping

```
