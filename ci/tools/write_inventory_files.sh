#!/bin/bash

# writes TESTING.yml on disk

if [ -f "$1" ]; then
  cat "$1" >> tests/group_vars/TESTING.yaml
else
  echo "ERROR: $1 is not a file!";
fi