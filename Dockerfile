ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}
LABEL maintainer="C. Guychard<christophe@article714.org>"

# Used Ansible Version
ARG ANSIBLE_VERSION


# Container tooling
COPY container /container

# Update env-var
ENV PATH=/home/ansible/scripts/:/usr/local/bin:${PATH}
ENV ANSIBLE_CONFIG=/container/config/ansible.cfg
ENV LANG=en_US.utf8
ENV LC_ALL=en_US.utf8

# Build container
RUN /container/build.sh

# Set default user when running the container
USER ansible