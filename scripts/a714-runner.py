#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

# Copyright: ©Article714 - 2021-2022
# Copyright: ©Tekfor - 2022

# GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)

import argparse
import string
from os import makedirs
from os.path import basename, exists, isdir, isfile, join, normpath, splitext
from shutil import rmtree
from uuid import uuid4

import yaml

try:
    from ansible_runner.interface import init_command_config
    from ansible_runner.utils import dump_artifact
except ImportError:
    print("ERROR: cannot find ansible_runner, stopping")
    exit(1)


def event_callback(data):
    print(f"EVENT: {data}")


def check_metadata(metadata, filename):
    """
    Ensure that metadata is compatible with Ansible714-runner
    """
    VALID_KEYWORDS = ("name", "description", "version", "ansible_recipes", "backup_node")
    VALID_RECIPE_PB_KEYWORDS = ("name", "type", "delegated", "hosts", "mode")
    MANDATORY_RECIPE_PB_KEYWORDS = ("name", "type")
    VALID_RECIPE_ROLE_KEYWORDS = ("name", "type", "delegated", "hosts", "mode",
                                  "become", "gather_facts")
    MANDATORY_RECIPE_ROLE_KEYWORDS = ("name", "type", "hosts")

    sname, ext = splitext(basename(filename))
    for key in metadata:
        if key not in VALID_KEYWORDS:
            print(f"WARNING: invalid metadata key: {key}")
            return False
    if " " in metadata.get("name", ""):
        print("WARNING: name should not contain space")
        return False
    else:
        name_chars_set = set(metadata.get("name", ""))
        special_chars_set = set(string.punctuation.replace("-", '').replace("_", ""))
        if name_chars_set.intersection(special_chars_set):
            print("WARNING: name should not contain special characters")
            return False
    if sname != metadata.get("name", ""):
        print("WARNING: filename should be same as service_name")
        return False

    for recipe in metadata.get("ansible_recipes", []):
        if recipe["type"] == "role":
            VALID_KW = VALID_RECIPE_ROLE_KEYWORDS
            MANDATORY_RECIPE_KEYWORDS = MANDATORY_RECIPE_ROLE_KEYWORDS
        elif recipe["type"] == "playbook":
            VALID_KW = VALID_RECIPE_PB_KEYWORDS
            MANDATORY_RECIPE_KEYWORDS = MANDATORY_RECIPE_PB_KEYWORDS
        for key in MANDATORY_RECIPE_KEYWORDS:
            if key not in recipe:
                print(f"WARNING: missing {key} in recipe {recipe['name']}")
                return False
        for key in recipe:
            if key not in VALID_KW:
                print(f"WARNING: invalid metadata key: {key} in recipe {recipe['name']} ")
                return False
    return True


def check_file_exists(filename):
    """
    Check if a file exists
    """
    if filename is None:
        return filename
    elif isfile(filename):
        return filename
    else:
        raise FileNotFoundError(f"Cannot find service file: {filename}")


def store_modes(modes):
    """
    split modes
    """
    available_modes = modes.split(',')
    if "none" in available_modes:
        available_modes.remove("none")
    return available_modes


def check_a714_file(filename):
    """
    checks if inventory and metada are consistent
    """
    def _fail_if(condition, message):
        """
        Helper function to fail when needed
        """
        if condition:
            print(f'FAILURE: {message}')
            exit(-1)

    # ------------
    # Check Metadata
    svc_data = read_svc_file(filename)
    _fail_if(not check_metadata(svc_data, filename), 'Provided metadata is not correct')

    # ------------
    # Check Inventory data
    try:
        from ansible.inventory.manager import InventoryManager
        from ansible.parsing.dataloader import DataLoader
        # from ansible.vars.manager import VariableManager
    except ImportError:
        _fail_if(True, "Could not import ansible packages")

    dl = DataLoader()
    im = InventoryManager(loader=dl, sources=[filename])
    # vm = VariableManager(loader=dl, inventory=im)
    all_hosts = im.get_hosts()
    # all_groups = im.list_groups()

    name = svc_data.get('name', None)
    backup_node = svc_data.get('backup_node', None)
    if backup_node:
        _fail_if(backup_node not in all_hosts,
                 f"Backup Node ({backup_node} is absent from inventory ({all_hosts}")

    print(f'Could manage service "{name}"')

    return 0


def check_directory_exists(somedirname, create=False):
    """
    Check if a directory exists
    """
    if somedirname is None:
        return somedirname
    elif not exists(somedirname) and create:
        makedirs(somedirname, mode=0o700)
    elif isdir(somedirname):
        return somedirname
    else:
        raise FileNotFoundError(f"Cannot find output directory  {somedirname}")


def read_svc_file(filename):
    """Reads service file and returns metadata (yaml)

    Args:
        filename (string): Service file name
    """

    if not isfile(filename):
        print("Error: the file is not a Ansible714 service file")
        exit(1)

    svc_data = None

    # ********************
    # Read the service file
    with open(filename, "r") as svcfile:
        svc_data = yaml.safe_load(svcfile)
        svc_data = svc_data.get("__METADATA__", {}).get("vars", {}).get("a714_metadata", None)
    return svc_data


def init_args():
    """
    Parse args for a714-runner command line
    """
    parser = argparse.ArgumentParser(description="Manage some host or service using Ansible714")
    mtx = parser.add_mutually_exclusive_group(required=True)
    filemtx = parser.add_mutually_exclusive_group(required=False)
    filemtx.add_argument(
        "--svc-file",
        dest="filename",
        default=None,
        type=check_file_exists,
        required=False,
        help="(deprecated) A service file",
    )
    filemtx.add_argument(
        "--file",
        dest="filename",
        default=None,
        type=check_file_exists,
        required=False,
        help="Ansible714 service or host definition file",
    )
    mtx.add_argument(
        "--output-dir",
        dest="outputdirname",
        default=None,
        type=check_directory_exists,
        required=False,
        help="Directory where to write artifacts & log files with detailed information",
    )
    parser.add_argument(
        "--no-ansible-output",
        dest="no_ansible_output",
        default=False,
        required=False,
        action="store_true",
        help="Removes ansible output to stdout",
    )
    mtx.add_argument(
        "--show-config",
        dest="dump_config",
        choices=["view", "dump", "metadata"],
        default=None,
        type=str,
        required=False,
        help="Use ansibe-config to [dump, view ]config, better than run command, "
        "or dumps metadata"
        " from service file [metadata]",
    )
    mtx.add_argument(
        "--check",
        dest="check_file",
        default=False,
        required=False,
        action="store_true",
        help="Check if provided file is correct",
    )
    parser.add_argument(
        "--verbosity",
        dest="verbose",
        default=0,
        type=int,
        required=False,
        help="Verbosity level",
    )
    parser.add_argument(
        "--modes",
        dest="modes",
        default=[],
        type=store_modes,
        required=False,
        help="A comma separated list of modes, used to filter recipe to apply",
    )
    parser.add_argument(
        "--dry",
        dest="dry",
        default=False,
        action="store_true",
        required=False,
        help="Dry mode: do not run anything but list which recipe would have run"
    )

    return parser.parse_args()


def generate_roleapply_playbook(rolename, path, become=True, gather_facts=True, hosts="all"):
    """
    Generates a playbook to apply a role
    returns filename
    """

    roleapplypb = [
        {
            "hosts": hosts,
            "gather_facts": gather_facts,
            "become": become,
            "name": f"Apply Role  {rolename}",
            "roles": [rolename],
        }
    ]

    filename = f"{str(uuid4().hex)}.yaml"

    playbookfile = dump_artifact(yaml.dump(roleapplypb), path, filename)

    return playbookfile


def my_run_command(
    executable_cmd,
    artifact_dir=None,
    no_ansible_ouptut=False,
    cmdline_args=None,
    **kwargs,
):
    """
    Command wrapper inspired by ansible-runner's code (run_command)
    """

    r = init_command_config(executable_cmd, cmdline_args=cmdline_args, **kwargs)

    r.config.suppress_ansible_output = no_ansible_ouptut
    if artifact_dir is not None:
        r.config.artifact_dir = artifact_dir
    # TODO Maybe one day
    # r.config.resource_profiling = True
    # r.config.resource_profiling_results_dir = "log"

    r.run()
    if artifact_dir is None:
        rmtree(r.config.artifact_dir, ignore_errors=True)
    return r


# ----------------------------------------------------------
#
# Main - Script Entry
#
if __name__ == "__main__":

    args = init_args()

    # ********************
    # dumps config and exits
    if args.dump_config:
        if args.dump_config == "metadata":
            if args.filename is None:
                print(
                    "a714-runner.py: error: displaying metadata requires service definition file"
                    "  (--svc-file) - Stopping "
                )
                exit(1)

            print(read_svc_file(args.filename))
            exit(0)
        else:
            arunr = my_run_command(
                executable_cmd="ansible-config",
                cmdline_args=[args.dump_config],
                runner_mode="subprocess",
            )
        exit(0)
    # ********************
    # check inventory file and exits
    if args.check_file:
        exit(check_a714_file(args.filename))

    if args.filename is None:
        print(
            "a714-runner.py: warning: cannot do anything without a service"
            "definition file  (--svc-file) - Stopping "
        )
        exit(1)
    svc_data = read_svc_file(args.filename)
    if not check_metadata(svc_data, args.filename):
        exit(1)

    # ********************
    # run recipes (roles, playbooks,...), eventually delegated to localhost when specified so
    if svc_data:
        recipe_index = 0
        logfile = None
        if args.modes:
            args.modes.append("any")
        else:
            args.modes.append("none")
        for recipe in svc_data["ansible_recipes"]:
            # check if mode is ok
            if not recipe.get("mode", "none") in args.modes:
                print(
                    f"Warning: ignoring recipe {recipe['name']} "
                    f"({recipe.get('mode', 'none')}) in current mode(s): {args.modes}"
                )
                continue
            elif args.dry:
                print(
                    f"Would have run recipe {recipe['name']} "
                    f"({recipe.get('mode', 'none')}) in current mode(s): {args.modes}"
                )
                continue

            # create output subdir
            output_subdir = normpath(
                join(
                    args.outputdirname,
                    f"{svc_data['name']}_recipe{recipe_index}",
                )
            )

            command_args = ["-i", args.filename]
            arunr = None
            hosts = None

            if "backup_node" in svc_data:
                command_args.append("--extra-vars")
                command_args.append(f"service_backup_node={svc_data.get('backup_node',False)}")

            if args.verbose:
                command_args.append("-" + "v" * args.verbose)

            if recipe.get("delegated", False):
                command_args.append("-c")
                command_args.append("local")
                command_args.append("-e")
                command_args.append("ansible_connection=local")

            if "hosts" in recipe:
                hosts = recipe["hosts"]
            else:
                hosts = "all"

            if recipe["type"] == "playbook":
                if hosts and hosts != "all":
                    command_args.append("-l")
                    command_args.append(hosts)
                command_args.append(recipe["name"])

            elif recipe["type"] == "role":
                # defaults
                role_gather_facts = True
                role_become = True
                # provided values
                if "gather_facts" in recipe:
                    role_gather_facts = recipe["gather_facts"]
                if "become" in recipe:
                    role_become = recipe["become"]

                command_args.append(
                    generate_roleapply_playbook(
                        recipe["name"],
                        output_subdir,
                        become=role_become,
                        gather_facts=role_gather_facts,
                        hosts=hosts,
                    )
                )
            else:
                print(
                    "Error: do not know how to deal with recipe "
                    f"({recipe['type']}) {recipe['name']} "
                )
                exit(1)

            # Let's go!
            arunr = my_run_command(
                executable_cmd="ansible-playbook",
                cmdline_args=command_args,
                runner_mode="subprocess",
                artifact_dir=output_subdir,
                no_ansible_ouptut=args.no_ansible_output,
                input_fd=None,
            )

            # process result
            if arunr:
                if arunr.rc > 0:
                    if recipe.get("delegated", False):
                        print(
                            "\nERROR: failed to execute delegated "
                            f"recipe ({recipe['type']}) {recipe['name']} [{recipe_index}]"
                        )
                    else:
                        print(
                            "\nERROR: failed to execute recipe "
                            f"({recipe['type']}) {recipe['name']}  [{recipe_index}]"
                        )
                    exit(arunr.rc)
                else:
                    if recipe.get("delegated", False):
                        print(
                            (
                                "\nSUCCESS: executed delegated recipe"
                                f" ({recipe['type']}) {recipe['name']}  [{recipe_index}]"
                            )
                        )
                    else:
                        print(
                            f"\nSUCCESS: executed recipe ({recipe['type']})"
                            f" {recipe['name']}  [{recipe_index}]")

                # re-init
                del arunr
                arunr = None

                # next recipe
                recipe_index += 1
        exit(0)
    else:
        print("Error: the file is not a Ansible714 service file")
        exit(1)
