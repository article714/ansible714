#/bin/bash -x

# set -x

PWD=$(pwd)
MY_UID=$(id -u)

: ${PYTHON_VERSION=3.10}
: ${DEBIAN_VERSION=slim-bullseye}


# clean
rm -Rf ${PWD}/documentation/site

# Detect if running on WSL

is_wsl=$(uname -a | grep microsoft | grep -c WSL)

# Build Docs
docker run -i -u ${MY_UID} --rm -v ${PWD}:/build python:${PYTHON_VERSION}-${DEBIAN_VERSION} /bin/bash -c "mkdir -p /build/builds;\
export HOME=/build/builds;\
export PATH=\${PATH}:\${HOME}/.local/bin;\
cd /build;\
/usr/local/bin/python -m pip install --upgrade pip;\
/usr/local/bin/python -m pip install --upgrade --user -r requirements_docs.txt;\
ansible-galaxy collection install -r galaxy_requirements.yml;\
./ci/tools/generate_collections_docs.sh;\
./ci/tools/generate_roles_docs.sh;\
mkdocs build;\
mv site documentation;"

if [ $? -ne 0 ]; then
    echo "Error, cannot continue docs building"
    exit 1
fi

# Build image
cd documentation
docker pull article714/resty-container:latest
docker build --force-rm --no-cache -t article714/ansible714-doc:latest .

if [ $? -ne 0 ]; then
    echo "Error, could not build Docker Image"
    exit 1
fi

# Start doc container
container_id=$(docker inspect -f '{{.Id}}' ansible714_doc 2>/dev/null)
if [ -n "${container_id}" ]; then
    docker stop ansible714_doc
    docker rm ansible714_doc
else
    echo "container does not exist"
fi

if [ $is_wsl -gt 0 ]; then
    # use port forwarding on Windows/WSL
    docker run -d --restart always -p 8080:8080 --name ansible714_doc article714/ansible714-doc:latest
    container_ip="127.0.0.1"

else
    # use container IP on Linux
    docker run -d --restart always --name ansible714_doc article714/ansible714-doc:latest
    container_ip=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ansible714_doc)
fi

# print doc URL

echo "******************************************************"
echo ""
echo "use: http://${container_ip}:8080 to access documentation"
echo ""
echo "******************************************************"
