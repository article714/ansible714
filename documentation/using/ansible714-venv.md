# Using a virtualenv

The following steps will provide you with a working Ansible714 tooling, that you can run inside its associated virtualenv.

Then, you'll have to customize your inventory in order that it fits your needs and is usefule to manage you own infrastructure.

### Used conventions

-   `$install_dir`, is the environment variable pointing at the directory where Ansible714 tooling will be installed,
-   `$inventory_dir`, is the environment variable pointing at the directory where your Ansible inventory will live.

## Create your virtualenv

1.  Install python tooling: `virtualenv`, `virtualenvwrapper` and `python3`

    /e.g. on a Debian or Ubuntu node:
    `apt-get install virtualenv virtualenvwrapper python3`

2.  Create your virtual python environment (_virtualenv_), named 'ansible714':

    `mkvirtualenv -p /usr/bin/python3 ansible714`

3.  Clone Ansible714 tooling:

    ```shell
    export install_dir='/to/be/customized'
    cd ${install_dir}
    git clone --single-branch --branch master https://gitlab.com/article714/ansible714
    ```

4.  Install all needed python packages inside your virtualenv:

    ```shell
    # get in the right directory
    workon ansible714
    export install_dir='/to/be/customized'
    cd ${install_dir}/ansible714

    # choose an ansible version to install
    export ANSIBLE_VERSION=6.0.0

    # update pip
    python -m pip install --upgrade pip

    # install Ansible714 runtime dependencies (python packages)
    python -m pip install --upgrade -r requirements.txt

    ```

5.  Get Roles & Collections Ansibleè14 depends on:

    ```shell
    #  get in the right directory
    workon ansible714
    export install_dir='/to/be/customized'
    cd ${install_dir}/ansible714

    # get Roles and Collections we depend on
    ./scripts/init_dependencies.sh ${install_dir}
    ```

## Initialize your inventory

Here, we use Ansible714 provided scripts to initialize the directory that will host your Ansible inventory.

_That's the directory inside which you will have to run your ansible commands_

1.  Create the inventory directory

    ```shell
    export inventory_dir="/to/be/customized/inventory"

    mkdir -p ${inventory_dir}
    ```

2.  Build directory layout

    ```shell
    ${install_dir}/ansible714/scripts/setup_ansible714_venv.sh ${inventory_dir}
    ```

3.  test that Ansible works

    ```shell
    # Which version?
    ansible --version

    # Can I ping some node (should fail ...)
    ansible all -m ping

    # Check your config
    ansible-config dump
    ```

In order for your Ansible14 to fit the need, you'll have to customize the sample files copied in you inventory directory to adapt them to your targeted environment.

## Run some playbooks

1.  Check that your configuration is consistent:

    -   in `${inventory_dir}/hosts.yml` file you should configure all your target nodes & groups
    -   `${inventory_dir}/host_vars`, should contain a file per node, containing node specific information.

2.  then start using playbooks:

    For example:

    ```shell
    # run the playbook that should update software  on server1 (should fail, as "server1" might not exist)

    export install_dir='/to/be/customized'
    export inventory_dir="/to/be/customized/inventory"
    cd ${inventory_dir}
    ansible-playbook ${install_dir}/ansible714/playbooks/update-software.yml -l server1
    ```
