# Release notes

## Version 0.2.2

- Fixed issue #116 about community.general.system.ufw internal ref
- Fixed issue #33, added handlers to let's encrypt role
- Fixed issue #115 about Let's Encrypt tests
- Refactored some CI Jobs
- Fixed issue #113, about import_role/include_role issues
- Updated documentation about testing in a local environment

## Version 0.2.1

- fixed missing `become` for resolvectrl flush-caches
- publish documentation to gitlab pages
- Added dependency to community.hashi_vault to enable secrets retrieval from Hashicorp Vault
- Added som documentation and scripts about testing on your local development computer
- Added a role to manage logstash installation (issue #104)
- Deprecated versions based on Ansible < 6.0.0 (PiPy package), (issue #99)
- Fixed several issues with locales
- Changed image naming for a more consistent versioning scheme
- Fixed issue #84 (rclone installation)
- Added dependency to openstack.cloud collection (issue #106)
- Added support for pre-commit (issue #107)
- Fixed issue #93, deprecation of `run_ansible714.sh` script
- Fix issue with Let's Encrypt certificate renewal: zone should not be remove from record
    retrieve by dnstxt
- Add retries when rsync fails (`backup_target` role)
- Dropped French translation (issue #97)
- Fixed an issue with `install-vpn` playbook: wrong file paths (issue #94)
- Using [ansible-doctor](https://ansible-doctor.geekdocs.de) to generate role documentation
    (issue #96)
- Fixed issue #88: "hosts" variable is now mandatory for "role" type recipes (ansible714-runner)
- Fixed issues #85, #89
- Update ansible-lint to latest available
- Drop python 2.7 support (convergence is too costly...)
- Move to bullseye for debian base distro
- Build image for several versions of Ansible
- Add support for rclone tool for backing up (`backup_target` role)
- Add new role to manage simple backup mechanisms: `backup_target` (issue #42)
- By default output ssh traffic (to port 22) is authorized when activating firewall for
    role `debian-based-host`
- `a714-runner`: add a way to specify on which host a playbook must be applied (`-l` option)
- Add `fail2ban` to any host using `debian-based-host` role
- Progressively Migrate playbooks/roles to prefixed module (Galaxy compatibility)
- Added an option to `docker-node` role to only install Docker client
- Added an option to `a714-runner` to filter applicable recipes (`--mode`)
- Added an option to `a714-runner` to check inventory file against Ansible714-runner
    rules (`--check`)
- `a714-runner`: changed option `--svc-file` (deprecated, but kept for pour compatibility),
    for `--file`
- Many documentation updates
- Add rclone et rsync utils into docker image (issue #71)
- Dependencies fixed due to ansible-lint 6.x release
- Fixed issue #69, issues with rights on a directory from inventory
- Add documentation for Ansible714 collection(s) to project documentation/site (issue #61)
- Merged debian-based-host & debian-based-container (kept for compatibility) (issue #49)
- Fixed default access right for Let's encrypt TLS/SSL certificates (directory traversal)
- Better tests and validation process, preparing for unit-testing of collection
- Fixed a task name error in haproxy-node role & implementation issue for HaProxy config
    directory deployment (issue #40)
- Add a new parameter to specify on which hosts a role might be applied (Issue #62)
- Add `ansible714-runner` support for `become` and `gather_facts` parameters when applying roles
- Add a dependency on a brand new collection: `ansible714.ovh` and needed python modules
- Modification of the SSH playbook configuration (replacement of the trusted ca keys file by a
    variable)(issue #54)
- Modification of the install-vpn playbook (we now copy folders and not specific files)
    (issue #55)
- Modification of the configure_ha task (we now copy the ha folder and not just a
    configuration file)(issue #40)
- Fixed issues #48, #52 and #53
- Added a command line option to `a714-runner` (--artifact-dir) to store ansible commands output
    and artifact files (a subdirectory per role/playbook)
- Added a command line option to `a714-runner` (--no-ansible-output) to suppress default Ansible
    output
- Added a command line option to `a714-runner` (--show-config) to dipsplay Ansible configuration
    being used
- Fixed a deprecation warning for docker_container module
- Added and documented a new `machine_type` _ovh-pci_ (issue #38)
- Adding a playbook to modify the SSH configuration (issue #30)

## Version 0.2.0

- Removed Proxmox related tooling (obsolete)
- Improved installation & initialization script
- Improvement: re-apply docker-node role after a software update (issue with docker listeners)
- Fixed syntax error in let's encrypt role
- Fixed missing parameter in edit_json
- Enabled default Gitlab's security scanners (IaC) on pipelines for Ansible714
- Added role for cups server management
- Added playbook to configure IpSec
- Added a parameter to enable reverse DNs entry update when managing a given DNS entry of 'A' type
- Added ansible714 scripts directory to container path
- Added a runner to process "service" inventory files
- Added new playbook to manage OVH's public cloud instances (issue #37)
- Switched to Article714's of infra-ovh-ansible-module, in order to benefit from evolutions
    needed to write
    playbooks to manage OVH Public Cloud instances
- Fixed bad use of synthesio.ovh.domain for let's encrypt role (issue #36)
- Fixed issue with default du ansible.cfg location (issue #35)
- More consistent playbook names
- Added a Galaxy module to manage mdraid arrays
- Improved linting for playbooks and roles
- Improved dependency management for ansible-role-systemd-networkd
- Fixed issue with docker configuration (empty daemon.json, issue #29)
- Fixed issue with acme_domains default value (issue #28)
- Added tests with a docker container as an Ansible target
- Added "preview" image publishing from "master" branch
- Updated dependency to infra-ovh-ansible-module (issue #14)

## Version 0.1.2

- fixed issue #24
- added the possibility of adjusting the logging configuration for Docker containers (driver &
    options) (issue #15)
- updated to python 3.9.6
- added dependencies needed to control the K8S instance (issue #13)
- fixed conflict between python version 3.9 and 3.7 when using ansible (issue #12)
- update to ansible 2.9.23
- Fixed the issue of renewing let's Encrypt certificates with http-01 challenge (issue #9)
- Addressed an installation problem with the docker-ce-cli package (issue #8)
- (Regression) Problem with the renewal of Let's Encrypt certificates following modification made
    in 0.1.1 (issue #6)
- The certificate renewal time can be configured using the _letsencrypt_renewal_delay_
    variable (issue #7)

## Version 0.1.1

- fixed release_notes (CHANGELOGS)
- suppressed the delay on updating the OVH DNS (replaced by a wait loop)
- Merge of ansible714 and ansible714-docker projects
- Adoption of standard Article714 rules for Docker images production
- Fixed issues with running container (gitlab-runner) as non-root user (ansible)

## Version 0.1.0

- Updated version of Python (3.9.2)
- Addition of a waiting loop in case of concern of DNS update at OVH
- Refactored the entire production chain using the [build-tools](https://gitlab.com/article714/build-tools)
    project
- Migrated official repositories to [Gitlab.com](https://gitlab.com/article714).

## Version 0.0.3 and earlier

Version 0.0.3 and earlier are "historical" versions produced before the clarification of how images were
produced and the move to [Gitlab.com](https://gitlab.com).
