# Ansible714 playbooks

-   [deploy-docker-containers](deploy-docker-containers.md)
-   [dns-updates](dns-updates.md)
-   [docker-node](docker-node.md)
-   [gitlabrunner-node](gitlabrunner-node.md)
-   [haproxy-node](haproxy-node.md)
-   [helm-setup-deploy](helm-setup-deploy.md)
-   [install-vpn](install-vpn.md)
-   [mailserver-node](mailserver-node.md)
-   [network-config](network-config.md)
-   [ovh-public-cloud-instance-setup](ovh-public-cloud-instance-setup.md)
-   [postgresqlserver-node](postgresqlserver-node.md)
-   [update-letsencrypt](update-letsencrypt.md)
-   [update-software](update-software.md)
-   [webserver-node](webserver-node.md)

## Deprecated/unmaintained playbooks

-   [proxyserver-node](proxyserver-node.md) \*

\*: should be replaced by [Ansible714 runner](../using/ansible714-runner.md) usage.

## Experimental playbooks

-   [prepare-check-mode](prepare-check-mode.md)
