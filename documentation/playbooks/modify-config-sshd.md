---
title: modify-config-sshd
tags:
    - sshd
    - playbook
---

# Modify the SSH configuration

This playbook allows you to add configuration lines to the **sshd_config** file. It also copies a file which name is given by the variable (**mytrusted_ca** here) to _/etc/ssh_ (if it exists)
