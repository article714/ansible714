# Supported ansible versions

Ansible714 tooling is built and tested for the following [Ansible community package](https://pypi.org/project/ansible/) versions
(as of Ansible714 version 0.2.1):

- 6.0.0, ansible-core version 2.12
- 6.5.0, ansible-core version 2.13
- 7.3.0, ansible-core version 2.14

The list of supported versions is specified in the `.gitlab-ci.yml` file and an image is produced for each supported Ansible version.

_If you want to have another version supported, please submit an issue or a merge request._
