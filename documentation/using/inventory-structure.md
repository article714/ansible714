# Setup an inventory

Ansible714 builds on [Ansible conventions](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html), and may use any Ansible inventory source without modificiation.

Yet, we encourage to use some additional conventions to clarify inventory organisation:

![SampleInventory](img/inventory_conventions.png "inventory"){ align=left width="300" }

-   subdirectories for hosting files used by playbooks or target specific files:

    -   `files`: directory containing one subdirectory per node named after Ansible target name in inventory (`inventory_hostname`);

    -   `certs`: SSL/TLS certificates used to connect on targets or to some other service;

    -   `keys`: SSL/TLS certificates used to connect on targets or to some other service (e.g. Let's Encrypt account key);

-   subdirectories for hosting inventory files:

    -   `group_vars`: _same as ansible_, directory containing 1 file, named after group name + `.yml` extension, used to define (yaml format) group variables;

    -   `host_vars`: _same as ansible_, directory containing 1 file per target node, named after Ansible target name in inventory (`inventory_hostname`) + `.yml` extension, used to define (yaml format) node variables;

    -   `hosts`: yaml files that define [Ansible714 hosts](services_hosts.md) variables and playbooks/roles to apply (_when using ansible714-runner_);

    -   `services`: yaml files that define [Ansible714 services](services_hosts.md) variables and playbooks/roles to apply (_when using ansible714-runner_).

_This is the directory structure created by Ansible714 initialization scripts._

**Some of the [playbooks](/playbooks/index.html) provided by Ansible714 might infere that your inventory is organized as previoulsy described.** In particular when looking (by default) for configuration files to install on target.

The most significant differences appear when using [ansible714-runner](ansible714-runner.md), as it needs some additional metadata in service or host inventory files.
