Targets Debian or derivative Linux distributions

Depending on [machine type](../using/machine_types.md), this role will act differently on target node.
In particular:

-   `ovh-pci` machines are defined as virtual machine hosted at OVH, running a Debian based OS, and using Netplan as a network configuration tool

## Default firewall rules

Out traffic authorized for
