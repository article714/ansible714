Title: Introducing Ansible714-runner

# Ansible714-runner

Inspired by [ansible-runner](https://ansible-runner.readthedocs.io/en/stable/)'s way of doing
things, but unwilling to deal with the complexity of building [input directories](https://ansible-runner.readthedocs.io/en/stable/intro/)
and projects, we designed an alternative system aiming at:

- define in a single file:

  - inventory elements (node(s), variables, group(s)) to be managed,

  - _ansible recipes_ to be used: playbooks to execute and roles to apply;

- keep things simple for file based inventories;

- enable a modularization of [inventory structure](inventory-structure.md).

## Using `a714-runner`

`a714-runner` is the runtime script that apply the previously exposed principles and use metadata
added to inventory files to decide what playbooks to execute or/role to apply on which nodes
(see example below).[Metadata reference](metadata_reference.md).

```yaml
# Ansible714 specific metadata
__METADATA__:
    vars:
        a714_metadata:
            name: My service name
            ansible_name: simple_svc # service "main" part
            version: 1.0
            ansible_recipes:
                - name: test_dns_loop.yml
                  type: playbook
                  delegated: true
                - name: haproxy-node
                  hosts: all
                  type: role
                  delegated: false
                # ... Some other playbooks or roles

# Ansible inventory data
MY_GROUP_OF_HOSTS:
    hosts:
        simple_svc:
            ansible_host: targetcontainer # node hosting service part
            ansible_connection: ssh
            ansible_user: someuser
            ansible_ssh_pass: somepassword
            ansible_become_password: somepassword
            ansible_python_interpreter: /usr/bin/python3
            machine_type: "container"
```
