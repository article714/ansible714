---
title: ovh-public-cloud-instance-setup
tags:

    - ovh
    - public cloud
    - playbook
    -
---

# Install and setup a VM instance on OVH Public Cloud

This playbook allows you to create and manage a public instance (VM) on OVH Public Cloud infrastructure.
It makes use of modules/roles in the [ansible714-ovh-collection](https://gitlab.com/article714/ansible714-ovh-collection).

Here is a sample host that is defined as a Public Cloud Instance:

```yaml
#...
hosts:
    some-test-instance:
        pci_project_name: MyPCIProject
        pci_instance_name: some-test-instance
        pci_flavor: d2-2
        pci_image_name: Ubuntu 20.04
        pci_region: SBG5
#...
```

Several data must be recovered to be able to use it:

- pci_project_name: name of the OVH Public Cloud project where instances are to be managed
- pci_instance_name: the name of the instance to manage
- ssh_key_id: the id of the ssh key used in OVH
- service_name: the id of the project that will host the instance
- flavor_id: the id of the flavor to use (cpu or gpu)
- region: the location of the instance
- image_id: the id of the intance image

Other data can be used, such as activating the monthly rental (here the rental is done by the hour).
Look [here](https://gitlab.com/article714/ansible714-ovh-collection)
