---
title: install-vpn
tags:
    - vpn
    - openvpn
    - network
---

# Install config VPN

This playbook allows you to configure an IPSec on a remote machine.

It simply copies the configuration files (ipsec.conf and ipsec.secrets) as well as the folders containing the different keys (folders _/private/_,_/certs/_ and _/cacerts/_) in the correct directories.

These files must therefore be prepared in advance.
