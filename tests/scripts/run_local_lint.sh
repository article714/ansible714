#!/bin/bash

# set -x

# setup used paths
LAUNCH_DIR=$(pwd)

MY_SCRIPT=$(realpath -s $0)
MY_SCRIPTPATH=$(dirname $MY_SCRIPT)
DOCKER_COMPOSE_PATH=$(realpath -s $MY_SCRIPTPATH/../docker)

# local test image identification
IMAGE_VERSION="localtest"
IMAGE_NAME=ansible714-docker
CI_COMMIT_BRANCH="__local__"

# Docker Env
test_network_name="a714_test_network"
test_storage_name="a714_test_storage"
project_name="ansible714"

# Docker Command
DOCKER_CMD="docker run -ti --rm -v $(pwd):/home/ansible/ansible714 article714/ansible714-docker:localtest /bin/bash -c "


${DOCKER_CMD} "
cd /home/ansible/ansible714;
python3 -m pip install -r requirements_dev.txt;

export PATH=/home/ansible/.local/bin:\$PATH;

ansible-lint -c .ansible-lint
"
