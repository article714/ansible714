#!/bin/bash

# set -x


# versions management (default when not set in environment)
: "${PYTHON_VERSION=3.10}"
: "${DEBIAN_VERSION=slim-bullseye}"
: "${ANSIBLE_VERSION=7.3.0}"

# local test image identification
IMAGE_VERSION="localtest"
IMAGE_NAME=ansible714-docker

# Docker build opts
DOCKER_ADDTL_PARAMS="--build-arg PYTHON_VERSION=${PYTHON_VERSION}-${DEBIAN_VERSION} --build-arg ANSIBLE_VERSION=${ANSIBLE_VERSION}"

#---------------
# build distro

tar czf container/ansible714.tar.gz modules playbooks plugins roles scripts ./*requirements*.*

#---------------
# Let's build image

echo " Building ${IMAGE_NAME}:${IMAGE_VERSION}"
# shellcheck disable=SC2086
docker build  -t "article714/${IMAGE_NAME}:${IMAGE_VERSION}" --build-arg IMAGE_VERSION="${IMAGE_VERSION}" ${DOCKER_ADDTL_PARAMS} -f Dockerfile_local .
