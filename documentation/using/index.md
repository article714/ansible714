# How to use Ansible714?

_If you want to contribute to/evolve Ansible714, you should read the [development doc](development.md)_

## Installing Ansible714 on your admin workstation (_control node_)

Two ways are supported to use Ansible714 from your admin workstation/[control node](https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node):

1. (_recommanded_) using [Docker image](ansible714-docker.md)
2. using a dedicated [virtualenv](ansible714-venv.md)

... starting from there, you can use any Ansible command, either from your virtualenv, or using the `run_ansible714.sh` [wrapper script](ansible714-docker.md#wrapper-script) provided for Docker.

## Ansible714 runner

In addition to the standard Ansible commands, Ansible714 provides an executor inspired by [Ansible runner](https://github.com/ansible/ansible-runner).

This new command uses a new deployment unit which contains both:

-   some inventory data
-   a list of playbooks and/or roles to apply on that inventory piece

This allows in particular to simplify the definition of services which can be deployed / administered in a single command: `a714-runner --svc-file mon_service.yml`.

`a714-runner` uses the libraries provided by the [Ansible runner](https://github.com/ansible/ansible-runner) project, and is the subject of [specific documentation](ansible714-runner.md)
