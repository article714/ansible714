---
title: docker-node
tags:
    - docker
    - playbook
---

# Manage Docker installation & daemon on a node
