---
title: webserver-node
tags:
    - webserver
    - playbook
    - haproxy
    - nginx
    - apache
---

# Manage webserver configuration

Complimentary to haproxy-node playbooks & roles
