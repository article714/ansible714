#!/bin/bash


#------------------------------------
show_usage() {
    echo "USAGE: setup_ansible714_venv.sh <directory name (full path)>"
}

if [ $# -eq 0 ] || [ $# -gt 1 ]; then
    show_usage
else
    target_dir="$1"
    #------------------ CHECKS --------------------
    if [ ! -d $target_dir ]; then
        echo "ERROR: target directory (${target_dir}) does not exist!"
        exit 1
    fi
    have_wget=$(type wget)
    if [ $? -gt 0 ]; then
        echo "ERROR: cannot find wget, needed to download initialisation content!"
        exit 1
    fi

    #------------------ Inventory directory LAYOUT ------------------------
    echo "Create inventory directory layout"
    mkdir -p ${target_dir}/files
    mkdir -p ${target_dir}/group_vars
    mkdir -p ${target_dir}/host_vars
    mkdir -p ${target_dir}/certs
    mkdir -p ${target_dir}/keys
    mkdir -p ${target_dir}/hosts
    mkdir -p ${target_dir}/services
    mkdir -p ${target_dir}/log


    #---------------------------------------------
    echo "Copy sample configuration files"
    cd ${target_dir}
    wget -q  https://gitlab.com/article714/ansible714/-/raw/master/documentation/examples/ansible.cfg
    wget -q  https://gitlab.com/article714/ansible714/-/raw/master/documentation/examples/hosts.yml


fi
