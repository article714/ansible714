---
title: gitlabrunner-node
tags:
    - docker
    - playbook
---

# Manage gitlab-runner installation on a node

Basicallyn this playbook applies [gitlabrunner-node role](/roles/gitlab-runner.md) on Ansible nodes belonging to the `GitlabrunnerNode` group.
