# Ansible714 development: testing

## Providing a test environment (Gitlab CI)

See [testing with OVH](testing-with-ovh.md).

## Testing on developer's computer

A Docker compose file is provided in order to setup a test environment. To setup this environment,
you need a working local docker installation,
including [docker-compose](https://docs.docker.com/compose/).

Test environment includes several containers that can be used as target nodes for running Ansible714
playbooks and tools:

* `targethost` & `targethost2`, containers to be used as Ansible nodes, targets for
  roles/playbooks, included into `tests/targehost_inventory.yml` inventory file;
* `targetcontainer`, target node to test `backup_target`role, included to  `tests/svc_to_backup.yml`
  inventory file (service);
* `pgsql_container`, a Postgresql container,  included to  `tests/svc_to_backup.yml`inventory
  file (service);
* `main_backup_node`, node used as centralized backup storage node;
* `ansible714_controller`

### Requirements

Before starting your local testing environment, you have to build an Ansible714-docker image for
local use.

A generation script is provided:

```bash
tests/scripts/build_local_test_image.sh
```

This script can be configured using environment variables:

* `PYTHON_VERSION`, python version to use, defaults to 3.10
* `DEBIAN_VERSION`, debian distro to use, defaults to bullseye-slim
* `ANSIBLE_VERSION`, Ansible version to use, defaults to 7.3.0

### Start test environment

You can start the tesg environment using Docker Compose:

```bash
docker-compose -f tests/docker/docker-compose.yml up -d
```

### Use test environment

Local git repos is mounted into Ansible714-controller container in `/builds` directory.

Run an interactive shell inside Ansible714 controller:

```bash
docker-compose -f tests/docker/docker-compose.yml exec  ansible714_controller /bin/bash
```

Run a playbook once inside Ansible714 controller:

```bash
cd /builds
# install logstash
ansible-playbook -v -i tests/targethost_inventory.yml tests/playbooks/test_mng_logstash.yml
# remove logstash
ansible-playbook -v -i tests/targethost_inventory.yml  --extra-vars "logstash_state=absent"  tests/playbooks/test_mng_logstash.yml
```

**IMPORTANT**: any Ansible714 sourcecode update (Role, playbook, plugin, dependencies,...),
you have to regenerate the local test Ansible714 image and update your Docker Compose test environment.

### Run a set of tests in a local environment

A script is provided, and may be adapted to simplify the running of a set of tests using a test environment
built from [previously cited](#start-test-environment) `docker-compose.yml`file.

`test/scrips/run_local_test.sh` is a (sample) script to run all the tests listed in the `tests` variable at the beginnning of
the script.
`tests` is a multi-line list, each line must be a valid shell command that will be executed on the controller started in
the test environment.

**NOTE**: in order for this script to work, the directory where you will checkout sourcecode (typically `ansible714`)
must have a `775` mode.
