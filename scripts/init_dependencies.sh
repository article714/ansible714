#!/bin/bash
# set -x

#------------------------------------
# check if a target directory is provided

if [ -z "$1" ]; then
    target_dir=$(pwd)
elif [ "$1" == "." ]; then
    target_dir=$(pwd)
else
    if [ ! -d "$1" ]; then
        mkdir -p "$1"
    fi
    target_dir="$1"
fi


#------------------------------------
# main script go


# get collections from galaxy
mkdir -p "${target_dir}"/collections
for retries in {1..5}; do
    ansible-galaxy collection install -r galaxy_requirements.yml --collections-path "${target_dir}"/collections
    # shellcheck disable=SC2181
    if [ $? -gt 0 ]; then
        echo "Could not install some collections from Galaxy, attempt number $retries";
        # shellcheck disable=SC2086
        if [ $retries -eq 5 ]; then
            exit 1
        fi
    else
        break
    fi
    sleep 5
done

# get roles from galaxy
mkdir -p "${target_dir}"/collections
for retries in {1..5}; do
    ansible-galaxy role install -r galaxy_requirements.yml --roles-path "${target_dir}"/roles
    # shellcheck disable=SC2181
    if [ $? -gt 0 ]; then
        echo "Could not install some roles from Galaxy, attempt number $retries";
        # shellcheck disable=SC2086
        if [ $retries -eq 5 ]; then
            exit 1
        fi
    else
        break
    fi
    sleep 5
done