## Credits

This role re-uses templates and configurations from [ansible-odoo](https://github.com/OCA/ansible-odoo) (GPL-V3 license)
