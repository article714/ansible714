This role aims at providing a simple way to backup Ansible714 [host(s) or service(s)](/using/services_hosts.html). In particular, when an Ansible714 Service is made of several parts that need to be backed-up, this

_How can we group all parts in a single Backup?_ In order to be able to restore the whole service in case of major failure or a provider change;
can we give a logical name to each part to ease backup storage/retrieval? Symbolic links?

## Conventions

-   **backup target**: is an Ansible node targeted by backup operations.
-   **backup node**: an Ansible node that stores backed-up data.

![conventions](img/backup_conventions.png "simple example"){width="800" }

## Operations

Standard Backup made in 2 steps

1. run backup operations on node(s) using local storage
2. sync (using rsync) local storage to a node that belongs to the group `BackupStorageNodes`

Backup retention directed by two variables:

-   `node_backup_retention_time`: defaults to `30d`
-   `backup_storage_retention_time`: defaults to `60d`

## Groups

-   `NodesToBackup`: Any node that will be backed-up

-   `BackupStorageNodes`: Nodes where to rsync backup files from other nodes

### Specific tools backups

-   `PostgresqlToBackup`: Node that hosts a postgresql server to backup

-   `DockerNodes`: Node that hosts some docker volumes to backup if part of `NodesToBackup`

## using rsync

Use Rsync to synchronize/backup some files from node to `BackupStorageNode`, using `backup_node`or `service_backup_node`\*\*(set from ansibke714-docker) values...

```yaml
use_rsync: true
```

## using rclone

Use [Rclone](https://rclone.org/) to synchronize/backup some files from node to some cloud storage solution.

To install Rclone, Ansible714 depends on `stefangweichinger.ansible_rclone` role.
So to use it, you have to refer to its [documentation](https://github.com/stefangweichinger/ansible-rclone).

```yaml
use_rclone: false
```

## "Basic" node backups

### Example: simple `/etc` backup

## Docker container backups

## Docker volume backups

## Postgresql backups

`node_backup_root_dir` is the path used to store backups... It is fixed... **TODO**: should change and document this far better.
